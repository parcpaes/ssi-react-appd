import React from 'react';
import {Breadcrumb, BreadcrumbItem, Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";
import Link from "react-router-dom/es/Link";
import CommentForm from './CommentForm';

function RenderItem({item}) {
    if (item != null) return (
        <Card>
            <CardImg width="100%" src={item.image} alt={item.name}/>
            <CardBody>
                <CardTitle>{item.name}</CardTitle>
                <CardText>{item.description}</CardText>
            </CardBody>
        </Card>
    );

    return (
        <div/>
    );
}

function RenderComments({comments, addComment, itemId}) {

    //data is a keyword of react
    if (comments != null) {
        const commentsItems = comments.map((datas) => {
            return (
                <ul className="list-unstyled" key={datas.id}>
                    <li>{datas.comment}</li>
                    <li>{`author: ${datas.author},
                        date:${new Intl.DateTimeFormat('en-US', {
                        year: 'numeric',
                        moth: 'short',
                        day: '2-digit'
                    }).format(new Date(Date.parse(datas.date)))}`}</li>
                </ul>
            );
        });

        return (
            <div>
                {commentsItems}
                <CommentForm itemId={itemId} addComment={addComment}/>
            </div>
        )
    }
    return <div/>;
}

const ItemDetail = (props) => {
    console.log('itemDetail rended is invoked ');
    //console.log(props.item.id);
    return (
        <div className="container">
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to='/catalog'>Catalog</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Cont us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{props.item.name}</h3>
                    <hr/>
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-xs-12 col-sm-12 col-md-5 m-1">
                    <RenderItem item={props.item}/>
                </div>
                <div className="col-12 col-xs-12 col-sm-12 col-md-5 m-1">
                    <h4>Comments</h4>
                    <RenderComments comments={props.comments} addComment={props.addComment} itemId={props.item.id}/>
                </div>
            </div>
        </div>
    );
}

export default ItemDetail;