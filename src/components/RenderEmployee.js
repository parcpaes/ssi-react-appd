import React from 'react';
import {Media} from "reactstrap";

const Employee = (props) => {
    const employees = props.employees.map((employee) => {
        return (
            <Media className="m-4" key={employee.id}>
                <Media left>
                    <Media style={{width: '120px'}} object data src={employee.image}/>
                </Media>
                <Media body>
                    <Media heading>{employee.name}</Media>
                    {employee.jobPosition}
                </Media>
            </Media>
        );
    });
    return (<div className="col-12 m-4">
        {employees}
    </div>);
}

export default Employee;