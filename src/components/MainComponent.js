import React, {Component} from 'react';
import Catalog from './CatalogComponent';
import ItemdDetail from './ItemdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent'
import Home from './HomeComponent';
import {Redirect, Route, Switch} from 'react-router-dom';
import Contact from './ContactComponent';
import About from './AboutComponent';
import {connect} from "react-redux";
import {addComment} from "../redux/ActionCreator";


class Main extends Component {
    constructor(props, context) {
        super(props, context);
        console.log('contructor main');
    }

    onItemSelect(itemid) {
        this.setState({
            selectedItem: itemid
        });
    }

    componentDidMount() {
        // console.log('Main componentDidMount es invocado');
    }

    render() {
        const itemDetailId = ({match}) => {
            return (
                <ItemdDetail
                    item={this.props.items.filter(item => item.id === parseInt(match.params.itemId))[0]}
                    comments={this.props.comments.filter(comment => comment.itemId === parseInt(match.params.itemId, 10))}
                    addComment={this.props.addComment}
                />
            )
        }
        const HomePage = () => {
            return (
                <Home
                    item={this.props.items.filter(item => item.featured)[0]}
                    employee={this.props.employees.filter(employee => employee.featured)[0]}
                />
            );
        }
        return (
            <div>
                <Header/>
                <Switch>
                    <Route path="/home" component={HomePage}/>
                    <Route path='/contact' component={Contact}/>
                    <Route exact path="/catalog" component={() => <Catalog items={this.props.items}/>}/>
                    <Route path='/catalog/:itemId' component={itemDetailId}/>
                    <Route path="/about" component={() => {
                        return <About employees={this.props.employees}/>
                    }}/>
                    <Redirect to="/home"/>
                </Switch>
                <Footer/>
            </div>
        );
    }
}

const mapStoreToProps = (state) => {
    return {
        items: state.items,
        employees: state.employees,
        comments: state.comments
    }
}

//del comonente a redux
// const mapDispatchToProps = dispatch => ({
//     addComment: (comment) => dispatch(addComment(comment))
// });

//export default withRouter(connect(mapStoreToProps,{addComment})(Main));

export default connect(mapStoreToProps, {addComment})(Main);