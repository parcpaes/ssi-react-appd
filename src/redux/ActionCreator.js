import * as ActionTypes from './ActionType';

//Action Add comments Comments
export const addComment = (comment) => ({
    type: ActionTypes.ADD_COMMENT,
    payload: comment
});