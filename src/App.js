import React, {Component} from 'react';
import './App.css';
import Main from './components/MainComponent';
import ITEMS from "./shared/items";
import {BrowserRouter} from 'react-router-dom';
import {ConfigureStore} from './redux/configureStore'

const store = ConfigureStore();
class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            items: ITEMS
        }
    }

    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <Main/>
                </div>
            </BrowserRouter>
        );
    }
}

export default App;
